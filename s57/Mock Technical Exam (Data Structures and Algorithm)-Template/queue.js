let collection = [];

// Write the queue functions below.
// Prints the elements in the queue
function print() {
	if (isEmpty()) return ([]) 
  	let str = '';
  	for (let i = 0; i < collection.length; i++) {
    	str += collection[i] + ', ';
  	}
  	console.log(str.trim());
}

// Adds an element to the end of the queue
function enqueue(element) {
  	collection[collection.length] = element;
  	return collection;
}

// Removes and returns the first element from the queue
function dequeue() {
    if (isEmpty()) {
    	return undefined;
  	}
  	let firstElement  = collection[collection.length - 1];
  	for (let i = 0; i < collection.length - 1; i++) {
    	collection[i] = collection[i + 1];
  	}
  	collection.length--;
  	//return firstElement;
  	let newfirstElement =[];
  	newfirstElement[0]=collection[0];
  	return newfirstElement;
}

// Removes and returns the last element from the queue
// function dequeue() {
//   if (isEmpty()) {
//     return undefined;
//   }
//   const lastElement = collection[collection.length - 1];
//   collection.length--;
//   return lastElement;
// }

// Returns the first element in the queue without removing it
function front() {
  	if (isEmpty()) {
    	return undefined;
  	}
  	return collection[0];
}

// Returns the size of the queue
function size() {
  	return collection.length;
}

// Checks if the queue is empty
function isEmpty() {
  	return collection.length === 0;
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty

};